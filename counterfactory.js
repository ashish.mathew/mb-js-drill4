function counterFactory(c)
{
  if (typeof(c) != "number")
  {
    throw Error("invalid argument");
  }
    let obj = {
        increment(){
           return ++c;
        },
        decrement(){
          return --c;
        }
    }
    return obj;
}
export {counterFactory};