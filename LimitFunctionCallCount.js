function limitFunctionCallCount(cb, n)
{
    if (typeof(cb) != "function" || typeof(n) != "number")
    {
        throw Error("invalid arguments");
    }
    function innerlimitcall()
    {
        for (let i = 0; i < n; i++)
        {
            cb(i);
        }
    }
    return innerlimitcall;
}
function call(no)
{
    console.log(no + 5);
}
export {limitFunctionCallCount,call};