function cacheFunction(cb)
{
    if(typeof(cb) != "function")
    {
        throw Error("invalid argument passed");
    }
    function invoke_cb(n)
    {
        if (n in cache)
        {
            console.log("cached result");
            return cache[n];
        }
        else
        {
            console.log("calculating");
            cache[n] = cb(n);
            return cb(n);
        }
    }
    let cache = {};
    return invoke_cb;
}
function multiply(n)
{
    return n * 2;
}
export {multiply,cacheFunction};