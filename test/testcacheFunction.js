import { cacheFunction,multiply } from "../cacheFunction.js";

try {
    let ans = cacheFunction(multiply)
    console.log(ans(5));
    console.log(ans(5));
    console.log(ans(10));
    console.log(ans(10));
} catch (error) {
    console.log(error);
}