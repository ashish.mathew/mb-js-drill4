import { counterFactory } from "../counterfactory.js";

try {
    var c = 4;
    var counter = counterFactory(c);
    console.log(counter.increment());
    console.log(counter.decrement());
} catch (error) {
    console.log(error);
}